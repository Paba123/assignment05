#include <stdio.h>

int atendee(float tprice){
    return 120-(tprice-15)*4;
}

float income(float tprice){
    return atendee(tprice)*tprice;
}

float cost(float tprice){
    return 500+3*atendee(tprice);
}

float profit(float tprice){
    return income(tprice)-cost(tprice);
}

int main(){
    float newtprice;
    printf("Enter the price: ");
    scanf("%f",&newtprice);

    printf("PROFIT: %.3f ",profit(newtprice));
    return (0);

}
